console.log('Software en linea')

var opinion = document.getElementById('opinion');
/* EXPORESIONES REGULARES*/
var expresiones_correo = /^\w+@\w+\.+[a-z]{2,3}$/; 
var expresiones_opinion =  /^.{10,100}$/;

opinion.onsubmit = function(){

    let correo = document.getElementById('correo').value;
    let opinion = document.getElementById('opinion').value;

    if(correo == "" || opinion == ""){
        alert("Algunos campos faltan por llenar");
        return false;
    }

    else if(!expresiones_correo.test(correo)){
        alert("Correo incorrecto");
        return false;
    }

    else if(!expresiones_opinion.test(opinion)){
        alert("Comentario demasiaso extenso");
        return false;
    }

    else{
        alert('Gracias por iniciar sesion con nosotros')
        return true;
    }
}